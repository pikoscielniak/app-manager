package modeler_test

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"app_manager/utils"
	"app_manager/modeler"
)

const configPath = "../testAssets/configs/workflowNotification.json"

var modelerParams = modeler.ModelerConfigParams{
	AppName:"WorkflowModeler",
	ApiUrl: "http://localhost/workflow/modeler/api/v1/",
}

func TestConfigReader(t *testing.T) {
	Convey("ConfigReader reads config", t, func() {
		err := utils.SaveJson(configPath, modelerParams)

		So(err, ShouldBeNil)

		config, err := modeler.ReadConfig(configPath)

		So(config.AppName, ShouldEqual, modelerParams.AppName)
		So(config.ApiUrl, ShouldEqual, modelerParams.ApiUrl)
	})
}
