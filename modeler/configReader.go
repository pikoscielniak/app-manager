package modeler
import (
	"app_manager/utils"
	"encoding/json"
)

func ReadConfig(path string) (ModelerConfigParams, error) {
	utils.PrintlnIn("READING MODELER CONFIG")
	var config ModelerConfigParams
	bytes, err := utils.ReadFile(path)
	if err != nil {
		return config, err
	}

	err = json.Unmarshal(bytes, &config)

	return config, err;
}