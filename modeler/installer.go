package modeler

import (
	"app_manager/installer"
	"app_manager/actions"
	"app_manager/utils"
)

const (
	RuntimeVersion = "v4.0"
	AppId = "workflowModeler"
)

type workflowModelerApp struct {
}

var app = workflowModelerApp{}

func Instance() workflowModelerApp {
	return app
}

func (app workflowModelerApp) RunInstall(args installer.CommandArgs) error {
	appConfig, err := ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	params := installer.InstallParams{
		AppName: appConfig.AppName,
		RuntimeVersion: RuntimeVersion,
		InstallPaths: installer.InstallPaths{
			AppSourcePath:args.AppPackagePath,
			WWWRootPath:args.WWWRootPath,
		},
	}
	result, err := actions.InstallIISAPP(params)
	if err != nil {
		return err
	}

	err = actions.InstallAppFromZip(args.AppPackagePath, result.PhysicalPath)
	if err != nil {
		return err
	}
	configPath := utils.Join(result.PhysicalPath, "appConfig.json")

	config := appConfigParams{
		BaseUrl: appConfig.ApiUrl,
		Environment:"production",
	}

	err = utils.SaveJson(configPath, config)
	if err != nil {
		return err
	}
	return nil
}

func (app workflowModelerApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app workflowModelerApp) AppId() string {
	return AppId
}
