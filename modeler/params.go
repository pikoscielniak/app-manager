package modeler

type ModelerConfigParams struct {
	AppName       string `json:"nazwaAplikacji"`
	ApiUrl        string `json:"urlDoApi"`
}

type appConfigParams struct {
	Environment string `json:"environment"`
	BaseUrl string `json:"baseUrl"`
}