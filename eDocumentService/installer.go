package eDocumentService

import (
	"app_manager/installer"
	"app_manager/actions"
	"app_manager/utils"
	"encoding/json"
)

const (
	AppId = "eDocumentService"
)

type eDcoumentServiceApp struct {
}

var app = eDcoumentServiceApp{}

func Instance() eDcoumentServiceApp {
	return app
}

type eDocumentServiceInstallParams struct {
	AppName        string `json:"nazwaAplikacji"`
	RuntimeVersion string `json:"wersjaFrameworka"`
	HangfireDb     installer.DbParams `json:"hangfireDb"`
	StorageDb      installer.DbParams `json:"storageDb"`
	AppSettings    []installer.AppSetting `json:"ustawieniaAplikacji"`
	installer.InstallPaths `json:"-"`
	Port           int `json:"port"`
}

func (app eDcoumentServiceApp) RunInstall(args installer.CommandArgs) error {
	installParams, err := readConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	installParams.AppSourcePath = args.AppPackagePath
	installParams.WWWRootPath = args.WWWRootPath

	result, err := actions.InstallIISAPP(installParams)
	if err != nil {
		return err
	}
	err = actions.InstallAppFromZip(installParams.AppSourcePath, result.PhysicalPath)
	if err != nil {
		return err
	}
	webConfigPath := utils.Join(result.PhysicalPath, utils.WebConfigFileName)

	connectionStringsParams := utils.ConnectionStringsParams{
		ConfigFilePath:webConfigPath,
		ConnectionsStrings: []installer.DbParams{installParams.HangfireDb, installParams.StorageDb},
	}
	err = utils.SetConnectionStrings(connectionStringsParams)

	err = utils.SetAppSettings(utils.AppSettingsParams{
		ConfigFilePath: webConfigPath,
		AppSettings: installParams.AppSettings,
	})
	return err
}

func readConfig(path string) (eDocumentServiceInstallParams, error) {
	var config eDocumentServiceInstallParams
	bytes, err := utils.ReadFile(path)
	if err != nil {
		return config, err
	}

	err = json.Unmarshal(bytes, &config)

	return config, err;
}

func (app eDcoumentServiceApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app eDcoumentServiceApp) AppId() string {
	return AppId
}

func (params eDocumentServiceInstallParams) InstallParams() *installer.InstallIISAppParams {
	return &installer.InstallIISAppParams{
		RuntimeVersion:params.RuntimeVersion,
		AppName:params.AppName,
		WWWRootPath: params.WWWRootPath,
	}
}
