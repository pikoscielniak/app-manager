package main

import (
	"app_manager/workflow"
	"fmt"
	"app_manager/installer"
	"app_manager/notification"
	"app_manager/modeler"
	"app_manager/utils"
	"flag"
	"encoding/json"
	"app_manager/actions"
	"app_manager/adminXE"
	"app_manager/adminXEIntegrationServicesProxy"
	"app_manager/eDocumentService"
)

type appManagerConfig struct {
	WwwRootPath string `json:"sciezkaDoWWWRoot"`
	WorkingDir  string `json:"sciezkaDoFolderuAplikacji"`
	FetchAppDir string `json:"sciezkaDoAktualizacjiAplikacji"`
}

type SupportedAppOption string

const version = "0.0.8"
const about = "AppManager wersja: " + version

const (
	actionHelp = "Dozowolne akcje: install, uninstall, fetch (pobiera nowe wersje aplikacji)"
	Workflow SupportedAppOption = workflow.AppId
	WorkflowNotification SupportedAppOption = notification.AppId
	WorkflowModeler SupportedAppOption = modeler.AppId
	AllWorkflow SupportedAppOption = "allWorkflow"
	AdminXE SupportedAppOption = adminXE.AppId
	AdminXEIntegrationServicesProxy SupportedAppOption = adminXEIntegrationServicesProxy.AppId
	AllAdmin SupportedAppOption = "allAdmin"
	EDocumentService SupportedAppOption = eDocumentService.AppId
	applicationHelp = "Dozwolone aplikacje: workflow, workflowNotification, workflowModeler, allWorkflow, " +
		"adminXE, adminXEIntegrationServicesProxy, allAdmin, eDocumentService"
	helpMessage = "Nieprawidlowe parametry. Wykonaj komende z poleceniem -h"
	configPathHelp = "Ścieżka do konfiguracji aplikacji"
	defaultConfigPath = "./appManager.json"
)

func parseConfig(configPath string) (appManagerConfig, error) {
	var config appManagerConfig
	bytes, err := utils.ReadFile(configPath)
	if err != nil {
		return config, err
	}
	err = json.Unmarshal(bytes, &config)

	return config, err;
}

func printHelp() {
	fmt.Println(helpMessage)
}

var optionToAppMap = map[SupportedAppOption][]installer.AppInstaller{
	Workflow: {workflow.Instance()},
	WorkflowNotification: {notification.Instance()},
	WorkflowModeler: {modeler.Instance()},
	AllWorkflow: {workflow.Instance(),
		notification.Instance(),
		modeler.Instance()},
	AdminXE: {adminXE.Instance()},
	//AdminXETestClient: {adminXETestClient.Instance()},
	//AdminXeWsFederationService: {adminXeWsFederationService.Instance()},
	AdminXEIntegrationServicesProxy: {adminXEIntegrationServicesProxy.Instance()},
	AllAdmin: {adminXE.Instance(), adminXEIntegrationServicesProxy.Instance()},
	EDocumentService: {eDocumentService.Instance()},
}

func prepareConfigPath(workingDir, configFileName string) string {
	return utils.Join(workingDir, "configs", configFileName + ".json")
}

func preparePackagePath(workingDir, appId string) string {
	return utils.Join(workingDir, "packages", appId, appId + ".zip")
}

func main() {
	println(about)

	var actionKind string;
	var appNameStr string
	var configPath string
	flag.StringVar(&actionKind, "o", "", actionHelp)
	flag.StringVar(&appNameStr, "a", "", applicationHelp)
	flag.StringVar(&configPath, "c", defaultConfigPath, configPathHelp)
	flag.Parse()

	installerCfg, err := parseConfig(configPath)

	if err != nil {
		fmt.Println(err)
		return;
	}

	utils.PrintlnIn("Working directory:", installerCfg.WorkingDir)
	utils.PrintlnIn("WWWRoot directory:", installerCfg.WwwRootPath)
	utils.PrintlnIn("Action", actionKind)
	utils.PrintlnIn("Application", appNameStr)

	appName := SupportedAppOption(appNameStr)
	var msg string;
	apps := optionToAppMap[appName]

	switch actionKind {
	case "install":
		install(apps, installerCfg)
		msg = "INSTALL"
	case "uninstall":
		uninstall(apps, installerCfg);
		msg = "UNINSTALL"
	case "fetch":
		fetching(installerCfg)
		msg = "FETCHING"
	default:
		printHelp()
		return;
	}
	fmt.Println(msg, "SUCCESS")
}

func install(apps []installer.AppInstaller, cfg appManagerConfig) {
	for _, app := range apps {
		configPath := prepareConfigPath(cfg.WorkingDir, app.AppId())
		appPackagePath := preparePackagePath(cfg.WorkingDir, app.AppId())
		utils.PrintlnIn("INSTALLING:", app.AppId())
		utils.PrintlnIn("CONFIG PATH:", configPath)
		utils.PrintlnIn("PACKAGE PATH:", appPackagePath)
		args := installer.CommandArgs{
			ConfigPath: configPath,
			WWWRootPath: cfg.WwwRootPath,
			AppPackagePath: appPackagePath,
		}
		err := app.RunInstall(args)
		if err != nil {
			fmt.Println(err)
			panic(err);
		}
		utils.PrintlnIn("INSTALLED:", app.AppId())
	}
}

func uninstall(apps []installer.AppInstaller, cfg appManagerConfig) {
	for _, app := range apps {
		configPath := prepareConfigPath(cfg.WorkingDir, app.AppId())
		appPackagePath := preparePackagePath(cfg.WorkingDir, app.AppId())
		utils.PrintlnIn("UNINSTALLING:", app.AppId())
		utils.PrintlnIn("CONFIG PATH:", configPath)
		utils.PrintlnIn("PACKAGE PATH:", appPackagePath)
		args := installer.CommandArgs{
			ConfigPath: configPath,
			WWWRootPath: cfg.WwwRootPath,
			AppPackagePath:appPackagePath,
		}
		err := app.RunUninstall(args)
		if err != nil {
			fmt.Println(err)
			panic(err);
		}
		utils.PrintlnIn("UNINSTALLED:", app.AppId())
	}
}

func fetching(cfg appManagerConfig) {
	params := installer.FetchApplicationsParams{
		SourcePath: cfg.FetchAppDir,
		DestinationPath:cfg.WorkingDir,
	}
	actions.FetchApplications(params)
}