package adminXETestClient

import (
	"app_manager/actions"
	"app_manager/installer"
	"app_manager/utils"
)

const (
	AppId = "adminXETestClient"
)

type adminXETestClientApp struct {

}

var app = adminXETestClientApp{}

func Instance() adminXETestClientApp {
	return app;
}
func (app adminXETestClientApp) RunInstall(args installer.CommandArgs) error {
	installParams, err := actions.ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	installParams.AppSourcePath = args.AppPackagePath
	installParams.WWWRootPath = args.WWWRootPath

	result, err := actions.InstallIISAPP(installParams)
	if err != nil {
		return err
	}
	err = actions.InstallAppFromZip(installParams.AppSourcePath, result.PhysicalPath)
	if err != nil {
		return err
	}
	webConfigPath := utils.Join(result.PhysicalPath, utils.WebConfigFileName)

	err = utils.SetAppSettings(utils.AppSettingsParams{
		ConfigFilePath: webConfigPath,
		AppSettings: installParams.AppSettings,
	})
	return err
}

func (app adminXETestClientApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app adminXETestClientApp) AppId() string {
	return AppId
}

