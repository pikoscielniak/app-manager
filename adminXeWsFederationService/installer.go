package adminXeWsFederationService

import (
"app_manager/actions"
"app_manager/utils"
"app_manager/installer"
)

const (
	AppId = "adminXeWsFederationService"
)

type adminXeWsFederationService struct {

}

var app = adminXeWsFederationService{}

func Instance() adminXeWsFederationService {
	return app
}
func (app adminXeWsFederationService) RunInstall(args installer.CommandArgs) error {
	installParams, err := actions.ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	installParams.AppSourcePath = args.AppPackagePath
	installParams.WWWRootPath = args.WWWRootPath

	result, err := actions.InstallIISSite(installParams)
	if err != nil {
		return err
	}

	err = actions.EnableWindowsAuthentication(installParams.AppName);
	if err != nil {
		return err
	}

	err = actions.InstallAppFromZip(installParams.AppSourcePath, result.PhysicalPath)
	if err != nil {
		return err
	}
	webConfigPath := utils.Join(result.PhysicalPath, utils.WebConfigFileName)

	err = utils.SetAppSettings(utils.AppSettingsParams{
		ConfigFilePath: webConfigPath,
		AppSettings: installParams.AppSettings,
	})
	return err
}

func (app adminXeWsFederationService) RunUninstall(args installer.CommandArgs) error {
	params, err := actions.ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	iisParams := installer.UninstallIISSiteParams{
		AppName:params.AppName,
		WWWRootPath:args.WWWRootPath,
	}
	return utils.UninstallIISSite(iisParams)
}

func (app adminXeWsFederationService) AppId() string {
	return AppId
}