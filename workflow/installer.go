package workflow
import (
	"app_manager/installer"
	"app_manager/actions"
)

const (
	AppId = "workflow"
)

type workflowApp struct {
}

var app = workflowApp{}

func Instance() workflowApp {
	return app
}

func (app workflowApp) RunInstall(args installer.CommandArgs) error {
	return actions.RunInstall(args)
}

func (app workflowApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app workflowApp) AppId() string {
	return AppId
}
