package adminXE

import (
	"app_manager/actions"
	"app_manager/installer"
)

const (
	AppId = "adminXE"
)

type adminXEApp struct {

}

var app = adminXEApp{}

func Instance() adminXEApp {
	return app;
}

func (app adminXEApp) RunInstall(args installer.CommandArgs) error {
	return actions.RunInstall(args)
}

func (app adminXEApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app adminXEApp) AppId() string {
	return AppId
}

