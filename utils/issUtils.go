package utils

import (
	"fmt"
	"os/exec"
	"app_manager/installer"
)

var appCmdPath = `C:\Windows\System32\inetsrv\AppCmd.exe`

const (
	WebConfigFileName = "Web.config"
)

type createIISAppParams struct {
	Name         string
	PhysicalPath string
	AppPoolName  string
}

func createIISApp(params createIISAppParams) error {
	siteNameArg := fmt.Sprintf("/site.name:%s", "Default Web Site")
	physicalPathArg := fmt.Sprintf(`/physicalPath:%s`, params.PhysicalPath)
	appPathArg := fmt.Sprintf("/path:/%s", params.Name)
	appPoolArg := fmt.Sprintf("/applicationPool:%s", params.AppPoolName)
	var cmd = exec.Command(appCmdPath, "add", "app", siteNameArg,
		appPathArg, physicalPathArg, appPoolArg)
	return executeCommand(cmd)
}

type ListIISAppsParams struct {
	SiteName string
}

func ListIISApps(params ListIISAppsParams) error {
	siteNameArg := fmt.Sprintf("/site.name:%s", params.SiteName)
	var cmd = exec.Command(appCmdPath, "list", "apps", siteNameArg)
	return executeCommand(cmd)
}

type deleteIssAppParams struct {
	SiteName string
	AppName  string
}

func deleteIssApp(params deleteIssAppParams) error {
	appArg := fmt.Sprintf("%s/%s", params.SiteName, params.AppName)
	var cmd = exec.Command(appCmdPath, "delete", "app", appArg)
	return executeCommand(cmd)
}

type createIISAppPoolParams struct {
	Name           string
	RuntimeVersion string
}

//appcmd add apppool /name:Sales /managedRuntimeVersion:v2.0 /managedPipelineMode:Integrated
//appcmd add apppool /name:Sales /managedRuntimeVersion:v2.0 /managedPipelineMode:Classic
func createIISAppPool(params createIISAppPoolParams) error {
	managedRuntimeVersionArg := fmt.Sprintf("/managedRuntimeVersion:%s", params.RuntimeVersion)
	nameArg := fmt.Sprintf("/name:%s", params.Name)
	var cmd = exec.Command(appCmdPath, "add", "apppool", nameArg, managedRuntimeVersionArg)
	return executeCommand(cmd)
}

type deleteIssAppPoolParams struct {
	Name string
}

func deleteIISAppPool(params deleteIssAppPoolParams) error {
	var cmd = exec.Command(appCmdPath, "delete", "apppool", params.Name)
	return executeCommand(cmd)
}

type stopIssAppPoolParams struct {
	Name string
}

func stopIISAppPool(params stopIssAppPoolParams) error {
	var cmd = exec.Command(appCmdPath, "stop", "apppool", params.Name)
	return executeCommand(cmd)
}

func combinePath(wwwRoot string, name string) string {
	PrintlnIn("PATH: ", wwwRoot + name)
	return Join(wwwRoot, name)
}

func InstallIISApp(params *installer.InstallIISAppParams) (*installer.InstallIISAppResult, error) {
	var installPath = combinePath(params.WWWRootPath, params.AppName)
	err := Exists(installPath)

	if err != nil {
		err = Mkdir(installPath)
	}

	if err != nil {
		return nil, err
	}

	err = createIISAppPool(createIISAppPoolParams{
		Name: params.AppName,
		RuntimeVersion: params.RuntimeVersion,
	})
	if err != nil {
		return nil, err
	}
	err = createIISApp(createIISAppParams{
		Name: params.AppName,
		PhysicalPath: installPath,
		AppPoolName: params.AppName,
	})
	result := &installer.InstallIISAppResult{
		PhysicalPath: installPath,
	}
	return result, err
}

func UninstallIISApp(params installer.UninstallIISAppParams) error {
	stopParams := stopIssAppPoolParams{
		Name:params.AppName,
	}

	err := stopIISAppPool(stopParams)

	if err != nil {
		fmt.Println("Pula aplikacja %s byla juz zatrzymana", params.AppName)
	}

	deleteAppParams := deleteIssAppParams{
		AppName: params.AppName,
		SiteName:"Default Web Site",
	}
	err = deleteIssApp(deleteAppParams)

	if err != nil {
		return err
	}
	physicalPath := Join(params.WWWRootPath, params.AppName)
	err = RemoveAll(physicalPath)
	if err != nil {
		return err
	}

	deletePoolParams := deleteIssAppPoolParams{
		Name: params.AppName,
	}
	err = deleteIISAppPool(deletePoolParams)
	if err != nil {
		return err
	}
	return err
}

type createIISSiteParams struct {
	SiteName     string
	Port         int
	PhysicalPath string
	AppPoolName  string
}

func createIISSite(params createIISSiteParams) error {
	siteNameArg := fmt.Sprintf("/name:%s", params.SiteName)
	bindings := fmt.Sprintf(`/bindings:http/*:%d:`, params.Port)
	appPathArg := fmt.Sprintf("/physicalPath:%s", params.PhysicalPath)
	var cmd = exec.Command(appCmdPath, "add", "site", siteNameArg, appPathArg, bindings)
	return executeCommand(cmd)
}

func InstallIISSite(params *installer.InstallIISSiteParams) (*installer.InstallIISSiteResult, error) {
	var installPath = combinePath(params.WWWRootPath, params.AppName)
	err := Exists(installPath)

	if err != nil {
		err = Mkdir(installPath)
	}

	if err != nil {
		return nil, err
	}

	err = createIISAppPool(createIISAppPoolParams{
		Name: params.AppName,
		RuntimeVersion: params.RuntimeVersion,
	})
	if err != nil {
		return nil, err
	}
	iisSiteParams := createIISSiteParams{
		SiteName: params.AppName,
		PhysicalPath: installPath,
		AppPoolName: params.AppName,
		Port: params.Port,
	}
	err = createIISSite(iisSiteParams)
	if err != nil {
		return nil, err
	}
	err = setApplicationPoolOfIISSite(iisSiteParams)
	if err != nil {
		return nil, err
	}
	result := &installer.InstallIISSiteResult{
		PhysicalPath: installPath,
	}
	return result, err
}

//APPCMD.exe set app "test" /applicationPool:"<APP_POOL_NAME_HERE>"
func setApplicationPoolOfIISSite(params createIISSiteParams) error {
	siteNameArg := fmt.Sprintf("%s/", params.SiteName)
	appPoolArg := fmt.Sprintf("/applicationPool:%s", params.AppPoolName)
	var cmd = exec.Command(appCmdPath, "set", "app", siteNameArg, appPoolArg)
	return executeCommand(cmd)
}

func UninstallIISSite(params installer.UninstallIISSiteParams) error {
	stopParams := stopIssAppPoolParams{
		Name:params.AppName,
	}

	err := stopIISAppPool(stopParams)

	if err != nil {
		fmt.Println("Pula aplikacja %s byla juz zatrzymana", params.AppName)
	}

	err = deleteIssSite(params.AppName)

	if err != nil {
		return err
	}
	physicalPath := Join(params.WWWRootPath, params.AppName)
	err = RemoveAll(physicalPath)
	if err != nil {
		return err
	}

	deletePoolParams := deleteIssAppPoolParams{
		Name: params.AppName,
	}
	err = deleteIISAppPool(deletePoolParams)
	if err != nil {
		return err
	}
	return err
}
func deleteIssSite(siteName string) error {
	var cmd = exec.Command(appCmdPath, "delete", "site", siteName)
	return executeCommand(cmd)
}

func EnableWindowsAuthentication(siteName string) error {
	var cmd = exec.Command(appCmdPath, "set", "config", siteName, "/section:windowsAuthentication", "/enabled:true", "/commit:apphost")
	return executeCommand(cmd)
}