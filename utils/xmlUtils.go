package utils

import (
	"github.com/beevik/etree"
)
func OpenDocument(filePath string) (*etree.Document, error) {
	doc := etree.NewDocument()
	err := doc.ReadFromFile(filePath)
	return doc, err
}