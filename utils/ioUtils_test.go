package utils_test

import (
	"testing"
	"app_manager/utils"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDirectoryUtils(t *testing.T) {
	Convey("Methods Mkdir, DirExists, RemoveAll works correctly", t, func() {

		path := "./testDir"

		err := utils.Mkdir(path)
		So(err, ShouldBeNil)

		err = utils.Exists(path)
		So(err, ShouldBeNil)

		err = utils.RemoveAll(path)
		So(err, ShouldBeNil)

		err = utils.Exists(path)
		So(err, ShouldNotBeNil)
	})
}
