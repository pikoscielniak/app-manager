package utils
import (
	"testing"
	"app_manager/installer"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSetAppSettingsSetsAppSettings(t *testing.T) {

	Convey("Given app settings params", t, func() {
		setAppSettingsParams := AppSettingsParams{
			ConfigFilePath: `./Web.config`,
			AppSettings: []installer.AppSetting{
				{Key:"edokumentPath", Value:"http://zos-srv2/eDokument2/workflow/v1/"},
				{Key:"notificationServer", Value:"http://zos-srv2/workflownotification"},
			},
		}
		Convey("App settings are set in Web.config", func() {
			err := SetAppSettings(setAppSettingsParams)
			So(err, ShouldBeNil)
		})
	})
}
