package utils_test

import (
	"testing"
	"app_manager/utils"
	. "github.com/smartystreets/goconvey/convey"
	"app_manager/installer"
)

var params = installer.InstallIISAppParams{
	WWWRootPath:`C:\inetpub\wwwroot\`,
	AppName:"TestApp",
	RuntimeVersion:"v4.0",
}

func TestInstallIISApp(t *testing.T) {

	Convey("InstallIISApp installs application", t, func() {

		result, err := utils.InstallIISApp(&params)
		expectedPath := utils.Join(params.WWWRootPath, params.AppName)

		So(result.PhysicalPath, ShouldEqual, expectedPath)
		So(err, ShouldBeNil)
	})
}


func TestUninstallIISApp(t *testing.T) {

	Convey("UninstallIISApp uninstalls application", t, func() {
		uninstallParams := installer.UninstallIISAppParams{
			AppName: params.AppName,
			WWWRootPath: params.WWWRootPath,
		}

		err := utils.UninstallIISApp(uninstallParams)

		So(err, ShouldBeNil)
	})
}
