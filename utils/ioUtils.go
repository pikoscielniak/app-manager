package utils

import (
	"os"
	"path/filepath"
	"fmt"
	"io"
	"io/ioutil"
	"encoding/json"
	"os/exec"
)

const Permissions = 0770

func Mkdir(path string) error {
	err := Exists(path)
	if err != nil {
		return os.Mkdir(path, Permissions)
	}
	return err;
}


func MkdirAllWithMode(path string, perm os.FileMode) error {
	err := Exists(path)
	if err != nil {
		return os.MkdirAll(path, Permissions)
	}
	return err;
}

func Exists(path string) error {
	_, err := os.Stat(path)
	return err;
}

func RemoveAll(path string) error {
	return os.RemoveAll(path)
}

func Join(elem ...string) string {
	return filepath.Join(elem...)
}

func PrintlnIn(a ...interface{}) {
	fmt.Println(a)
}

func ParentDir(path string) string {
	return filepath.Dir(path)
}

func OpenFile(name string, flag int, perm os.FileMode) (*os.File, error) {
	return os.OpenFile(name, flag, perm)
}

func Copy(dst io.Writer, src io.Reader) (written int64, err error) {
	return io.Copy(dst, src)
}

func WriteFile(filename string, data []byte, perm os.FileMode) error {
	return ioutil.WriteFile(filename, data, perm)
}

func Remove(path string) error {
	return os.Remove(path)
}

func ReadFile(fileName string) ([]byte, error) {
	return ioutil.ReadFile(fileName);
}

func SaveJson(path string, data interface{}) error {
	blob, err := json.Marshal(data)
	if err != nil {
		return err
	}
	return WriteFile(path, blob, Permissions)
}

func CopyDirectoryWindowsOnly(source, dest string) error {
	PrintlnIn(source, dest)
	var cmd = exec.Command("xcopy", source, dest + "\\", "/Y", "/E")
	return executeCommand(cmd)
}

func executeCommand(cmd *exec.Cmd) error {
	r, err := cmd.Output()
	if (err != nil) {
		PrintlnIn(err)
	}
	PrintlnIn(string(r))
	return err
}
