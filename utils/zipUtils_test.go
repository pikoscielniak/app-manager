package utils_test
import (
	"testing"
	"app_manager/utils"
	. "github.com/smartystreets/goconvey/convey"
)

var testDest = `./Test`
var testArchive = `../testAssets/packages/testApp.zip`

func TestZipUnzipsArchive(t *testing.T) {

	Convey("ZipUnzipsArchive", t, func() {
		err := before()

		So(err, ShouldBeNil)

		err = utils.Unzip(testArchive, testDest)

		So(err, ShouldBeNil)

		err = after()

		So(err, ShouldBeNil)
	})
}

const testDir = "../utils"
const outputPath = "./AppPack.zip"

func TestZip(t *testing.T) {
	Convey("ZipDirectory", t, func() {
		Convey("zips directory", func() {
			err := utils.RemoveAll(outputPath)

			So(err, ShouldBeNil)

			err = utils.ZipDirectory(testDir, outputPath)

			So(err, ShouldBeNil)
		})
	})
}

func before() error {
	err := utils.RemoveAll(testDest)
	if err != nil {
		return err
	}
	err = utils.Mkdir(testDest)
	return err
}

func after() error {
	return utils.RemoveAll(testDest)
}