package utils
import (
	"github.com/beevik/etree"
	"fmt"
	"app_manager/installer"
)


const (
	appSettingsXPath = "//configuration/appSettings"
	keyAttrName = "key"
	valueAttrName = "value"
)

func getPathFor(key string) string {
	return fmt.Sprintf("./%s[@%s='%s']", addElemTag, keyAttrName, key)
}

type AppSettingsParams struct {
	ConfigFilePath string
	AppSettings    []installer.AppSetting
}

func addSettingTags(e *etree.Element, params AppSettingsParams) {
	for _, p := range params.AppSettings {
		path := getPathFor(p.Key)
		addElem := e.FindElement(path)
		if (addElem == nil) {
			addElem = e.CreateElement(addElemTag)
			addElem.CreateAttr(keyAttrName, p.Key)
		}
		addElem.CreateAttr(valueAttrName, p.Value)
	}
}

func SetAppSettings(params AppSettingsParams) error {
	doc, err := OpenDocument(params.ConfigFilePath)

	if err != nil {
		return err
	}

	appSettingsElem := doc.FindElement(appSettingsXPath)
	if appSettingsElem == nil {
		panic("AppSettings tag does not exist")
	}
	addSettingTags(appSettingsElem, params)

	err = doc.WriteToFile(params.ConfigFilePath)
	return err
}