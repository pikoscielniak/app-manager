package utils

import (
	"github.com/beevik/etree"
	"fmt"
	"app_manager/installer"
)
//<add name="WorkflowContext"
// connectionString="server=localhost; user id=pkoscielniak; password=1234; database=workflow"
// providerName="Npgsql"/>
//<add name="WorkflowContext"
// connectionString="Data Source=zos-srv-teamcity\sqlexpress;Initial Catalog=workflow;MultipleActiveResultSets=True;User Id=workflow;Password=workflow;"
// providerName="System.Data.SqlClient" />-->

const (
	connStringXPath = "//configuration/connectionStrings"
	addElemTag = "add"
	nameAttr = "name"
	connectionStringAttr = "connectionString"
	providerNameAttr = "providerName"
)

var dbProviders = map[installer.DatabaseEngine]string{
	installer.MsSql:"System.Data.SqlClient",
	installer.Postgres:"Npgsql",
}

var dbConnStringFormats = map[installer.DatabaseEngine]string{
	installer.MsSql:`Data Source=%s;Initial Catalog=%s;MultipleActiveResultSets=True;User Id=%s;Password=%s;`,
	installer.Postgres:"server=%s; database=%s; user id=%s; password=%s",
	installer.PostgresHangfire:"server=%s; database=%s; user id=%s; password=%s; Pooling=false",
}

type connectionStringCreationFunction func(*etree.Element, ConnectionStringParams) *etree.Element

var connectionStringCreationFunctionsMap = map[installer.DatabaseEngine]connectionStringCreationFunction{
	installer.MsSql:createConnStringAddElemWithProvider,
	installer.Postgres:createConnStringAddElemWithProvider,
	installer.PostgresHangfire:createConnStringTag,
}

type ConnectionStringParams struct {
	ConfigFilePath string
	Name           string
	installer.DbParams
}

func formatConnectionString(params ConnectionStringParams) string {
	return fmt.Sprintf(dbConnStringFormats[params.DbEngine],
		params.DbServer,
		params.DbName,
		params.DbUser,
		params.DbPassword,
	)
}

func createConnStringTag(e *etree.Element, params ConnectionStringParams) *etree.Element {
	connStringVal := formatConnectionString(params)

	addElem := e.CreateElement(addElemTag)
	addElem.CreateAttr(nameAttr, params.Name)
	addElem.CreateAttr(connectionStringAttr, connStringVal)

	return addElem
}

func createConnStringAddElemWithProvider(e *etree.Element, params ConnectionStringParams) *etree.Element {
	addElem := createConnStringTag(e, params)
	addElem.CreateAttr(providerNameAttr, dbProviders[params.DbEngine])
	return addElem
}

func createConnString(e *etree.Element, params ConnectionStringParams) *etree.Element {
	creator := connectionStringCreationFunctionsMap[params.DbEngine]
	return creator(e, params)
}

func clearConnStringsChildren(connStrings *etree.Element) {
	for _, connStr := range connStrings.SelectElements(addElemTag) {
		connStrings.RemoveChild(connStr)
	}
}

func SetConnectionString(params ConnectionStringParams) error {
	doc, err := OpenDocument(params.ConfigFilePath)

	if err != nil {
		return err
	}

	conStringsElem := doc.FindElement(connStringXPath)
	clearConnStringsChildren(conStringsElem)
	createConnString(conStringsElem, params)

	err = doc.WriteToFile(params.ConfigFilePath)
	return err
}

type ConnectionStringsParams struct {
	ConfigFilePath     string
	ConnectionsStrings []installer.DbParams
}

func SetConnectionStrings(params ConnectionStringsParams) error {
	doc, err := OpenDocument(params.ConfigFilePath)

	if err != nil {
		return err
	}

	conStringsElem := doc.FindElement(connStringXPath)
	clearConnStringsChildren(conStringsElem)

	for _, p := range params.ConnectionsStrings {

		dbParams := ConnectionStringParams{
			ConfigFilePath: params.ConfigFilePath,
			Name: p.DbContextName,
			DbParams: p,
		}
		createConnString(conStringsElem, dbParams)
	}

	err = doc.WriteToFile(params.ConfigFilePath)
	return err
}