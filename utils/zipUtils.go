package utils
import (
	"archive/zip"
	"github.com/jhoonb/archivex"
	"os"
)
//A dist directory must exists
func Unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer func() {
		if err := r.Close(); err != nil {
			panic(err)
		}
	}()

	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				panic(err)
			}
		}()

		path := Join(dest, f.Name)

		if f.FileInfo().IsDir() {
			err = MkdirAllWithMode(path, f.Mode())
			if (err != nil) {
				return err
			}
		} else {
			parentDir := ParentDir(path)
			if err = Exists(parentDir); err != nil {
				err = MkdirAllWithMode(parentDir, f.Mode())
				if err != nil {
					return err
				}
			}

			destFile, err := OpenFile(path, os.O_WRONLY | os.O_CREATE | os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := destFile.Close(); err != nil {
					panic(err)
				}
			}()

			_, err = Copy(destFile, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}
	for _, f := range r.File {
		PrintlnIn("FILE: ", f.Name)
		err := extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}
	return nil
}

func ZipDirectory(source string, destination string) error {
	zipArch := new(archivex.ZipFile)
	defer zipArch.Close()

	err := zipArch.Create(destination)
	if err != nil {
		return err
	}
	defer zipArch.Close()

	err = zipArch.AddAll(source, false)
	if err != nil {
		return err
	}
	return err
}
