package utils
import (
	"testing"
	"github.com/beevik/etree"
	"app_manager/installer"
	. "github.com/smartystreets/goconvey/convey"
)

var connStringParams = ConnectionStringParams{
	ConfigFilePath:`..\testAssets\Web.config`,
	Name: "WorkflowContext",
	DbParams: installer.DbParams{
		DbEngine:installer.Postgres,
		DbServer:`zos-srv-teamcity\workflow`,
		DbName:"workflow",
		DbUser:"workflowUser",
		DbPassword:"1234",
	},
}

func TestWorks(t *testing.T) {

	Convey("SetConnectionString works without errors", t, func() {
		err := SetConnectionString(connStringParams)
		So(err, ShouldBeNil)
	})
}

func TestConnectionStringForPostgresIsCorrect(t *testing.T) {

	Convey("formatConnectionString returns properly formated connection string", t, func() {
		connStr := formatConnectionString(connStringParams)
		expectedConStr := `server=zos-srv-teamcity\workflow; database=workflow; user id=workflowUser; password=1234`

		So(connStr, ShouldEqual, expectedConStr)
	})
}

func TestConnectionStringForMsSqlIsCorrect(t *testing.T) {

	Convey("Connection string for MsSql is correct", t, func() {

		connStringParams.DbEngine = installer.MsSql
		connStr := formatConnectionString(connStringParams)
		expectedConStr := `Data Source=zos-srv-teamcity\workflow;Initial Catalog=workflow;MultipleActiveResultSets=True;User Id=workflowUser;Password=1234;`

		So(connStr, ShouldEqual, expectedConStr)
	})
}

func TestAddElementIsCorrect(t *testing.T) {

	Convey("createConnStringAddElem adds connection string element", t, func() {
		doc := etree.NewDocument()
		doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)
		doc.CreateElement("root")
		elem := createConnStringAddElem(doc.Root(), connStringParams)

		nameAttr := elem.SelectAttr(nameAttr)
		So(nameAttr.Value, ShouldEqual, connStringParams.Name)

		connStringAttr := elem.SelectAttr(connectionStringAttr)
		expectedConnString := formatConnectionString(connStringParams)
		So(connStringAttr.Value,ShouldEqual,expectedConnString)

		providerAttr := elem.SelectAttr(providerNameAttr)
		So(providerAttr.Value, ShouldEqual,dbProviders[connStringParams.DbEngine])
	})
}