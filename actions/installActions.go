package actions
import (
	"app_manager/installer"
	"app_manager/utils"
)

func RunInstall(args installer.CommandArgs) error {

	installParams, err := ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	installParams.AppSourcePath = args.AppPackagePath
	installParams.WWWRootPath = args.WWWRootPath
	return Install(installParams)
}

func Install(params installer.InstallParams) error {

	result, err := InstallIISAPP(params)
	if err != nil {
		return err
	}
	webConfigPath := utils.Join(result.PhysicalPath, utils.WebConfigFileName)

	err = InstallAppFromZip(params.AppSourcePath, result.PhysicalPath)
	if err != nil {
		return err
	}

	err = SetConnectionString(params, webConfigPath)

	err = utils.SetAppSettings(utils.AppSettingsParams{
		ConfigFilePath: webConfigPath,
		AppSettings: params.AppSettings,
	})

	return err
}
func InstallAppFromZip(appSource string, appDest string) error {
	utils.PrintlnIn("UNZIPPING APPPLICATION")
	utils.PrintlnIn("Source", appSource, "; dest:", appDest)
	return utils.Unzip(appSource, appDest)
}

func SetConnectionString(params installer.InstallParams, webConfigPath string) error {
	connStringParams := utils.ConnectionStringParams{
		ConfigFilePath:webConfigPath,
		Name: params.DbContextName,
		DbParams: params.DbParams,
	}
	return utils.SetConnectionString(connStringParams)
}


