package actions_test

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"app_manager/installer"
	"app_manager/actions"
	"app_manager/utils"
)

const (
	sourceDir = "../testAssets/"
	destDir = "../testAssets/dest"
)

func TestFetchApplications(t *testing.T) {
	Convey("FetchApplications", t, func() {
		Convey("Fetches application when destination is empty", func() {

			err := before()
			params := installer.FetchApplicationsParams{
				SourcePath: sourceDir,
				DestinationPath: destDir,
			}

			err = actions.FetchApplications(params)

			So(err, ShouldBeNil)

			err = utils.Exists(utils.Join(destDir, installer.PackagesFolderName, "workflow", "workflow.zip"))

			So(err, ShouldBeNil)
		})

		Convey("Fetches application when destination is not empty", func() {

			params := installer.FetchApplicationsParams{
				SourcePath: sourceDir,
				DestinationPath: destDir,
			}

			err := actions.FetchApplications(params)

			So(err, ShouldBeNil)

			err = utils.Exists(utils.Join(destDir, installer.PackagesFolderName, "workflow", "workflow.zip"))

			So(err, ShouldBeNil)
		})
	})
}

func before() error {

	err := utils.RemoveAll(destDir)
	if err != nil {
		return err
	}
	err = utils.MkdirAllWithMode(utils.Join(sourceDir, installer.PackagesFolderName), 0770)

	if err != nil {
		return err
	}

	return utils.MkdirAllWithMode(destDir, 0770)
}