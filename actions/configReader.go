package actions

import (
	"app_manager/utils"
	"encoding/json"
	"app_manager/installer"
)

func ReadConfig(path string) (installer.InstallParams, error) {
	var config installer.InstallParams
	bytes, err := utils.ReadFile(path)
	if err != nil {
		return config, err
	}

	err = json.Unmarshal(bytes, &config)

	return config, err;
}