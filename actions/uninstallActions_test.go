package actions_test
import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"app_manager/installer"
	"app_manager/actions"
)


func TestUninstallUninstallsApplication(t *testing.T) {

	Convey("Uninstall uninstalls application", t, func() {
		uninstallParams := installer.UninstallParams{
			WWWRootPath: wwwRootPath,
			AppName: installParams.AppName,
		}
		err := actions.Uninstall(uninstallParams)

		So(err, ShouldBeNil)
	})
}