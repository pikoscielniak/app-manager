package actions
import (
	"app_manager/installer"
	"app_manager/utils"
)


func FetchApplications(params installer.FetchApplicationsParams) error {
	source := utils.Join(params.SourcePath, installer.PackagesFolderName)
	dest := utils.Join(params.DestinationPath, installer.PackagesFolderName)

	return utils.CopyDirectoryWindowsOnly(source, dest)
}