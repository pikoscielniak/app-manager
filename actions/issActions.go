package actions

import (
	"app_manager/installer"
	"app_manager/utils"
)

func InstallIISAPP(params installer.ISSInstallParamsProvider) (*installer.InstallIISAppResult, error) {
	issParams := params.InstallParams()

	return utils.InstallIISApp(issParams)
}

func InstallIISSite(params installer.InstallParams) (*installer.InstallIISSiteResult, error) {

	issParams := &installer.InstallIISSiteParams{
		RuntimeVersion:params.RuntimeVersion,
		AppName:params.AppName,
		WWWRootPath: params.WWWRootPath,
		Port:params.Port,
	}

	return utils.InstallIISSite(issParams)
}

func EnableWindowsAuthentication(siteName string) error {
	return utils.EnableWindowsAuthentication(siteName)
}