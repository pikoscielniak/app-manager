package actions_test
import (
	"testing"
	"app_manager/utils"
	. "github.com/smartystreets/goconvey/convey"
	"app_manager/installer"
	"app_manager/actions"
)
var installParamsForReader = installer.InstallParams{
	AppName:"WorkflowTest",
	RuntimeVersion:"v4.0",
	DbParams: installer.DbParams{
		DbContextName:"WorkflowContext",
		DbEngine: installer.Postgres,
		DbServer:`zos-srv-teamcity\workflow`,
		DbName:"workflow",
		DbUser:"workflowUser",
		DbPassword:"1234",
	},
	AppSettings: []installer.AppSetting{
		{Key:"edokumentPath", Value:"http://zos-srv2/eDokument2/workflow/v1/"},
		{Key:"notificationServer", Value:"http://zos-srv2/workflownotification"},
	},
}
const configPathReader = "../testAssets/configs/workflow.json"

func TestName(t *testing.T) {

	Convey("workflow.ReadConfig reads install config", t, func() {

		err := utils.SaveJson(configPathReader, installParamsForReader)
		So(err, ShouldBeNil)

		config, err := actions.ReadConfig(configPathReader)

		So(err, ShouldBeNil)

		So(config.AppSettings, ShouldHaveLength, 2)

		setting0 := config.AppSettings[0]
		expectedSetting0 := installParamsForReader.AppSettings[0]
		So(setting0.Key, ShouldEqual, expectedSetting0.Key)
		So(setting0.Value, ShouldEqual, expectedSetting0.Value)


		setting1 := config.AppSettings[1]
		expectedSetting1 := installParamsForReader.AppSettings[1]
		So(setting1.Key, ShouldEqual, expectedSetting1.Key)
		So(setting1.Value, ShouldEqual, expectedSetting1.Value)

		So(config.AppSourcePath, ShouldEqual, installParamsForReader.AppSourcePath)
		So(config.DbEngine, ShouldEqual, installParamsForReader.DbEngine)
		So(config.DbName, ShouldEqual, installParamsForReader.DbName)
		So(config.DbServer, ShouldEqual, config.DbServer)
		So(config.DbUser, ShouldEqual, installParamsForReader.DbUser)

		So(config.DbPassword, ShouldEqual, installParamsForReader.DbPassword)
		So(config.AppName, ShouldEqual, installParamsForReader.AppName)
	})
}