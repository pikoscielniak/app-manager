package actions
import (
	"app_manager/utils"
	"app_manager/installer"
)

func RunUninstall(args installer.CommandArgs) error {

	params, err := ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	uninstallParams := installer.UninstallParams{
		AppName: params.AppName,
		WWWRootPath: args.WWWRootPath,
	}
	return Uninstall(uninstallParams)
}

func Uninstall(params installer.UninstallParams) error {
	iisParams := installer.UninstallIISAppParams{
		AppName:params.AppName,
		WWWRootPath:params.WWWRootPath,
	}
	err := utils.UninstallIISApp(iisParams)
	return err
}
