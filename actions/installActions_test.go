package actions_test

import (
	"testing"
	"app_manager/utils"
	. "github.com/smartystreets/goconvey/convey"
	"app_manager/actions"
	"app_manager/installer"
)

var installParams = installer.InstallParams{
	AppName:"WorkflowTest",
	RuntimeVersion:"v4.0",
	DbParams: installer.DbParams{
		DbContextName:"WorkflowContext",
		DbEngine: installer.Postgres,
		DbServer:`zos-srv-teamcity\workflow`,
		DbName:"workflow",
		DbUser:"workflowUser",
		DbPassword:"1234",
	},
	AppSettings: []installer.AppSetting{
		{Key:"edokumentPath", Value:"http://zos-srv2/eDokument2/workflow/v1/"},
		{Key:"notificationServer", Value:"http://zos-srv2/workflownotification"},
	},
	InstallPaths: installer.InstallPaths{
		AppSourcePath: `../testAssets/packages/workflowTest/WorkflowTest.zip`,
		WWWRootPath: wwwRootPath,
	},
}

const wwwRootPath = `C:\inetpub\wwwroot\`

const configPathInstall = "../testAssets/configs/workflowTest.json"

func TestInstallInstallsApplication(t *testing.T) {

	Convey("Instal installs application", t, func() {

		err := utils.SaveJson(configPathInstall, installParams)
		So(err, ShouldBeNil)

		params, err := actions.ReadConfig(configPathInstall)

		So(err, ShouldBeNil)

		params.AppSourcePath = installParams.AppSourcePath
		params.WWWRootPath = wwwRootPath
		err = actions.Install(params)

		So(err, ShouldBeNil)
	})
}
