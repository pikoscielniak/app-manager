package installer

type DatabaseEngine string

const (
	MsSql DatabaseEngine = "MsSql"
	Postgres DatabaseEngine = "Postgres"
	PostgresHangfire DatabaseEngine = "PostgresHangfire"
	PackagesFolderName = "packages"
)

type DbParams struct {
	DbContextName string `json:"nazwaKontekstuBazyDanych"`
	DbEngine      DatabaseEngine `json:"silnikBazyDanych"`
	DbServer      string    `json:"serverBazyDanych"`
	DbName        string        `json:"nazwaBazyDanych"`
	DbUser        string        `json:"uzytkownikBazyDanych"`
	DbPassword    string        `json:"hasloDoBazyDanych"`
}

type AppSetting struct {
	Key   string
	Value string
}

type InstallPaths struct {
	AppSourcePath string `json:"-"`
	WWWRootPath   string `json:"-"`
}

type InstallParams struct {
	AppName        string `json:"nazwaAplikacji"`
	RuntimeVersion string `json:"wersjaFrameworka"`
	DbParams
	AppSettings    []AppSetting `json:"ustawieniaAplikacji"`
	InstallPaths `json:"-"`
	Port           int `json:"port"`
}

type UninstallParams struct {
	WWWRootPath string
	AppName     string
}

type CommandArgs struct {
	ConfigPath     string
	WWWRootPath    string
	AppPackagePath string
}

type AppInstaller interface {
	RunInstall(CommandArgs) error
	RunUninstall(CommandArgs) error
	AppId() string
}

type InstallIISAppParams struct {
	WWWRootPath    string
	AppName        string
	RuntimeVersion string
}

type InstallIISSiteParams struct {
	WWWRootPath    string
	AppName        string
	RuntimeVersion string
	Port           int
}

type UninstallIISAppParams struct {
	AppName     string
	WWWRootPath string
}

type UninstallIISSiteParams struct {
	AppName     string
	WWWRootPath string
}

type InstallIISAppResult struct {
	PhysicalPath string
}

type InstallIISSiteResult struct {
	PhysicalPath string
}

type ISSInstallParamsProvider interface {
	InstallParams() *InstallIISAppParams
}

func (params InstallParams) InstallParams() *InstallIISAppParams {
	issParams := InstallIISAppParams{
		RuntimeVersion:params.RuntimeVersion,
		AppName:params.AppName,
		WWWRootPath: params.WWWRootPath,
	}
	return &issParams
}

type FetchApplicationsParams struct {
	SourcePath      string
	DestinationPath string
}