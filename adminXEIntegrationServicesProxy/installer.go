package adminXEIntegrationServicesProxy

import (
	"app_manager/actions"
	"app_manager/installer"
	"app_manager/utils"
)

const (
	AppId = "adminXEIntegrationServicesProxy"
)

type adminXEIntegrationServicesProxyApp struct {

}

var app = adminXEIntegrationServicesProxyApp{}

func Instance() adminXEIntegrationServicesProxyApp {
	return app;
}
func (app adminXEIntegrationServicesProxyApp) RunInstall(args installer.CommandArgs) error {
	installParams, err := actions.ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	installParams.AppSourcePath = args.AppPackagePath
	installParams.WWWRootPath = args.WWWRootPath

	result, err := actions.InstallIISAPP(installParams)
	if err != nil {
		return err
	}
	err = actions.InstallAppFromZip(installParams.AppSourcePath, result.PhysicalPath)
	if err != nil {
		return err
	}
	webConfigPath := utils.Join(result.PhysicalPath, utils.WebConfigFileName)

	err = utils.SetAppSettings(utils.AppSettingsParams{
		ConfigFilePath: webConfigPath,
		AppSettings: installParams.AppSettings,
	})
	return err
}

func (app adminXEIntegrationServicesProxyApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app adminXEIntegrationServicesProxyApp) AppId() string {
	return AppId
}


