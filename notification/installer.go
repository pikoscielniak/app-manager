package notification
import (
	"app_manager/installer"
	"app_manager/actions"
)

const (
	AppId = "workflowNotification"
)

type workflowNotificationApp struct {
}

var app = workflowNotificationApp{}

func Instance() workflowNotificationApp {
	return app
}

func (app workflowNotificationApp) RunInstall(args installer.CommandArgs) error {
	installParams, err := actions.ReadConfig(args.ConfigPath)
	if err != nil {
		return err
	}
	installParams.AppSourcePath = args.AppPackagePath
	installParams.WWWRootPath = args.WWWRootPath

	result, err := actions.InstallIISAPP(installParams)
	if err != nil {
		return err
	}
	err = actions.InstallAppFromZip(installParams.AppSourcePath, result.PhysicalPath)
	if err != nil {
		return err
	}
	return err
}

func (app workflowNotificationApp) RunUninstall(args installer.CommandArgs) error {
	return actions.RunUninstall(args)
}

func (app workflowNotificationApp) AppId() string {
	return AppId
}
